# citra-ufork
Unofficial fork to maintain it, update it's libraries and solve bugs.
Dump your own games to play on the emulator, is the only right way.
This work is entirely free as in freedom and price. I'll never get any single $ from doing this.

System requirements:
    
    Minimum:
    - Android: dual core cortex a72 cpu at 2.2ghz, opengles 3.2 and a 200GFlops mali gpu
    - PC: dual core cpu with 400 single thread points score in geekbench 5 and sse3 instructions + 300GFlops gpu with opengl 4.3 or vulkan 1.1

    Recommended:
    - Android: dual core cortex a78 cpu at 2.2ghz, vulkan 1.3 and a 500GFlops adreno gpu
    - PC: dual core cpu with 1000 single thread points score in geekbench 5 and avx instructions + 1TFlop gpu with vulkan 1.3

Things done:
1. Use dynarmic-ufork
2. Update most of the external libraries
3. Test and apply last canary changes (one commit was buggy so it wasn't added)
4. Added wayland support with melonds method

Building instructions:
Linux: 
1. Download the source as a .zip file
2. Extract it with file-roller or other extractor.
3. Go to the build emu's directory.
4. Open that directory on the terminal.
5. cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
6. ninja

Note: You'll need to install ccache and ninja, if not, remove -G ninja and the ccache parts of the command and use make instead of ninja in step 6.
Clang is supported too.

Windows build with msys2 (30min to 1h):

1. install msys2 from: https://www.msys2.org/
2. run it
3. pacman -Syuu
4. reopen msys2 with clang64 environment
5. pacman -Syuu
6. pacman -S --needed git make p7zip pactoys
7. pacboy -S --needed toolchain:p ccache:p cmake:p ninja:p qt6:p qt6-multimedia:p qt6-multimedia-wmf:p qt6-tools:p qt6-translations:p clang:p openmp:p lld:p
8. git clone https://gitlab.com/Miguel-hrvs/citra-ufork.git
9. cd citra-ufork/build
10. cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER=clang++
11. ninja 
12. ninja bundle (you'll get the binaries with it's libraries in build/bundle)
13. If you get missing libraries launching the emu, you'll have to copy them from the C:msys2/clang64/bin folder to the citra-ufork/build/bundle folder.
These were the ones that I needed: libc++.dll libomp.dll libpng16-16.dll libmd4c.dll libharfbuzz-0.dll libfreetype6.dll zlib1.dll libicuin74.dll
libicuuc74.dll libb2-1.dll libdouble-conversion.dll libpcre2-16-0.dll libzstd.dll libbrotlidec.dll libglib-2.0-0.dll
libintl-8.dll libgraphite2.dll libbz2-1.dll libicudt74.dll libiconv-2.dll libbrotlicommon.dll libpcre2-8.0.dll

Windows build using arch linux (to speed up the process you could avoid the aur for some packages using the ownstuff repo):
Note: gcc usually has problems to build citra for windows, I recommend the msys2 method
1. Install the dependencies:
sudo pacman -S gtk-doc mingw-w64 mingw-w64-gcc qt6-base qt6-multimedia qt6-tools
qt6-declarative mingw-w64-binutils

from the aur (I used yay):
mingw-w64-cmake mingw-w64-rust mingw-w64-harfbuzz mingw-w64-fribidi mingw-w64-meson
mingw-w64-gdk-pixbuf2 mingw-w64-glib2 mingw-w64-libthai
mingw-w64-fontconfig mingw-w64-pixman mingw-w64-lzo
mingw-w64-freetype2 mingw-w64-librsvg mingw-w64-poppler
mingw-w64-graphite mingw-w64-graphite2 mingw-w64-openjpeg2 
mingw-w64-cairo mingw-w64-curl mingw-w64-glib2 mingw-w64-icu  
mingw-w64-libdatrie mingw-w64-wine mingw-w64-qt6-base 
mingw-w64-qt6-shadertools mingw-w64-qt6-declarative mingw-w64-qt6-tools    
mingw-w64-qt6-multimedia mingw-w64-sdl2 mingw-w64-portaudio mingw-w64-ldd mingw-w64-spirv-tools mingw-w64-spirv-tools


If you get a circular dependency: Use mingw-w64-freetype2-bootstrap to 
break the circle; build mingw-w64-freetype2-bootstrap, 
mingw-w64-cairo-bootstrap, mingw-w64-pango
mingw-w64-harfbuzz and mingw-w64-freetype2 in that order. 
You can get rid of the bootstrapping package afterwards.

2. For step 5 do this: x86_64-w64-mingw32-cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
-DCMAKE_CXX_COMPILER=i686-w64-mingw32-g++ -DCMAKE_C_COMPILER=i686-w64-mingw32-gcc
3. ninja 

If you get a linking error you'll have to use clang with mingw

Build android apk with arch linux:

1. install these packages with pacman:
java-environment-common jdk-openjdk hiredis gradle lib32-gcc-libs lib32-gcc-libs java-runtime android-udev
2. install these packages from the aur:
android-sdk-cmdline-tools-latest android-sdk-build-tools sdkmanager
3. go to the android directory in the src folder
4. make a file called local.properties and write:
sdk.dir=/opt/android-sdk
5. chmod +x ./gradlew
6. sudo ./gradlew assemblenightlyRelease 
Note: if you get a license error, run sudo sdkmanager --licenses
7. sudo ./gradlew bundlenightlyRelease